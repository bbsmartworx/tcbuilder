ARCHS = v2i v3 v4 v4i

.NOTPARALLEL:

all: $(ARCHS)

$(ARCHS):
	$(MAKE) -f Makefile.$@

clean:
	rm -rf x86_64
