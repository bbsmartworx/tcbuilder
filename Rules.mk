TOOLCHAIN_NAME = gcc-icr-$(ICR_PLATFORM)-$(TARGET_MACH)

BUILD_DIR      = /opt/toolchain/build-$(TOOLCHAIN_NAME)
INSTALL_DIR    = /opt/toolchain/$(TOOLCHAIN_NAME)
SYSROOT_DIR    = $(INSTALL_DIR)/sysroot
PACKAGE_DIR    = packages

DEB_CONTROL    = $(BUILD_DIR)/$(TOOLCHAIN_NAME)/DEBIAN/control
RPM_SPEC       = $(BUILD_DIR)/$(TOOLCHAIN_NAME).spec

PKG_BINUTILS   = binutils-$(BINUTILS_VERSION).tar.gz
PKG_KERNEL     = linux-$(LINUX_VERSION).tar.gz
PKG_GCC        = gcc-$(GCC_VERSION).tar.gz
PKG_GLIBC      = glibc-$(GLIBC_VERSION).tar.gz

MAINTAINER     = Lukáš Chmela <lukas.chmela@advantech.com>

.NOTPARALLEL:

all: sanity-check unpack patch build strip pack clean

sanity-check:
	@if [ -d $(BUILD_DIR) ]; then \
	  echo "Removing existing directory \"$(BUILD_DIR)\"."; \
	  rm -rf $(BUILD_DIR); \
	fi
	@if [ -d $(INSTALL_DIR) ]; then \
	  echo "Refusing to overwrite contents of existing directory \"$(INSTALL_DIR)\"." \
	       "Please uninstall the source DEB or RPM package first or remove" \
	       "the directory manually if it was not removed completely." >&2; \
	  exit 1; \
	fi

$(PACKAGE_DIR)/$(PKG_BINUTILS):
	wget -P $(PACKAGE_DIR) https://ftp.gnu.org/gnu/binutils/$(PKG_BINUTILS)

$(PACKAGE_DIR)/$(PKG_KERNEL):
	wget -P $(PACKAGE_DIR) https://www.kernel.org/pub/linux/kernel/v$(shell echo $(LINUX_VERSION) | head -c 1).x/$(PKG_KERNEL)

$(PACKAGE_DIR)/$(PKG_GCC):
	wget -P $(PACKAGE_DIR) https://ftp.gnu.org/gnu/gcc/gcc-$(GCC_VERSION)/$(PKG_GCC)

$(PACKAGE_DIR)/$(PKG_GLIBC):
	wget -P $(PACKAGE_DIR) https://ftp.gnu.org/gnu/glibc/$(PKG_GLIBC)

unpack: unpack-binutils unpack-kernel unpack-gcc unpack-glibc

unpack-binutils: $(PACKAGE_DIR)/$(PKG_BINUTILS)
	mkdir -p $(BUILD_DIR)
	tar xfz $^ -C $(BUILD_DIR)

unpack-kernel: $(PACKAGE_DIR)/$(PKG_KERNEL)
	mkdir -p $(BUILD_DIR)
	tar xfz $^ -C $(BUILD_DIR)

unpack-gcc: $(PACKAGE_DIR)/$(PKG_GCC)
	mkdir -p $(BUILD_DIR)
	tar xfz $^ -C $(BUILD_DIR)

unpack-glibc: $(PACKAGE_DIR)/$(PKG_GLIBC)
	mkdir -p $(BUILD_DIR)
	tar xfz $^ -C $(BUILD_DIR)

patch: patch-gcc

patch-gcc:
	sed -e "s/-lmpc -lmpfr -lgmp/-Wl,-Bstatic,-lmpc,-lmpfr,-lgmp,-Bdynamic/" -i $(BUILD_DIR)/gcc-$(GCC_VERSION)/configure

build: build-binutils build-kernel-headers build-gcc-static build-glibc build-gcc

build-binutils:
	mkdir -p $(BUILD_DIR)/binutils
	cd $(BUILD_DIR)/binutils && \
	$(BUILD_DIR)/binutils-$(BINUTILS_VERSION)/configure \
	--target=$(TARGET_MACH) \
	--prefix=$(INSTALL_DIR) \
	--with-sysroot=$(SYSROOT_DIR) \
	--disable-nls \
	--disable-werror && \
	$(MAKE) -j8 LDFLAGS="-s" && \
	$(MAKE) install

build-kernel-headers:
	cd $(BUILD_DIR)/linux-$(LINUX_VERSION) && \
	$(MAKE) mrproper && \
	$(MAKE) ARCH=$(TARGET_ARCH) defconfig && \
	$(MAKE) ARCH=$(TARGET_ARCH) INSTALL_HDR_PATH=$(SYSROOT_DIR)/usr headers_install

build-gcc-static:
	mkdir -p $(BUILD_DIR)/gcc-static
	cd $(BUILD_DIR)/gcc-static && \
	$(BUILD_DIR)/gcc-$(GCC_VERSION)/configure \
	--target=$(TARGET_MACH) \
	--prefix=$(INSTALL_DIR) \
	--without-headers \
	--enable-bootstrap \
	--enable-languages=c,c++ \
	--enable-__cxa_atexit \
	--enable-libatomic \
	--disable-libcilkrts \
	--disable-libgcj \
	--disable-libgomp \
	--disable-libitm \
	--disable-libmudflap \
	--disable-libquadmath \
	--disable-libssp \
	--disable-libstdc++-v3 \
	--disable-libvtv \
	--disable-lto \
	--disable-multilib \
	--disable-nls \
	--disable-shared \
	--disable-threads \
	$(TARGET_OPTS) && \
	$(MAKE) -j8 LDFLAGS="-s" all-gcc && \
	$(MAKE) -j8 all-target-libgcc && \
	$(MAKE) install-gcc install-target-libgcc

build-glibc:
	mkdir -p $(BUILD_DIR)/glibc
	echo "slibdir=/lib" >> $(BUILD_DIR)/glibc/configparms
	export PATH=$(PATH):$(INSTALL_DIR)/bin && \
	cd $(BUILD_DIR)/glibc && \
	$(BUILD_DIR)/glibc-$(GLIBC_VERSION)/configure \
	--host=$(TARGET_MACH) \
	--prefix=/usr \
	--libdir=/usr/lib \
	--with-headers=$(SYSROOT_DIR)/usr/include \
	--enable-kernel=$(LINUX_VERSION) \
	--disable-profile \
	--disable-werror \
	libc_cv_forced_unwind=yes \
	libc_cv_ctors_header=yes \
	libc_cv_c_cleanup=yes && \
	$(MAKE) -k install-headers cross_compiling=yes install_root=$(SYSROOT_DIR) && \
	$(MAKE) -j8 LDFLAGS="-s" && \
	$(MAKE) install_root=$(SYSROOT_DIR) install

build-gcc:
	mkdir -p $(BUILD_DIR)/gcc
	export CC=gcc && \
	cd $(BUILD_DIR)/gcc && \
	$(BUILD_DIR)/gcc-$(GCC_VERSION)/configure \
	--target=$(TARGET_MACH) \
	--prefix=$(INSTALL_DIR) \
	--with-sysroot=$(SYSROOT_DIR) \
	--enable-languages=c,c++ \
	--enable-__cxa_atexit \
	--enable-threads=posix \
	--enable-libatomic \
	--disable-libgomp \
	--disable-libmudflap \
	--disable-libquadmath \
	--disable-libstdcxx-pch \
	--disable-lto \
	--disable-multilib \
	--disable-nls \
	$(TARGET_OPTS) && \
	libc_cv_forced_unwind=yes \
	libc_cv_ctors_header=yes \
	libc_cv_c_cleanup=yes && \
	$(MAKE) -j8 LDFLAGS="-s" all-gcc && \
	$(MAKE) -j8 SYSROOT_CFLAGS_FOR_TARGET="-s" all-target-libstdc++-v3 && \
	$(MAKE) -j8 SYSROOT_CFLAGS_FOR_TARGET="-s" all-target-libsanitizer && \
	$(MAKE) -j8 SYSROOT_CFLAGS_FOR_TARGET="-s" all-target-libatomic && \
	$(MAKE) install-gcc install-target-libstdc++-v3 install-target-libsanitizer install-target-libatomic

strip:
	rm -rf $(INSTALL_DIR)/share/info
	rm -rf $(INSTALL_DIR)/share/man
	rm -rf $(INSTALL_DIR)/sysroot/usr/share/info

pack: pack-deb pack-rpm

pack-deb:
	mkdir -p x86_64
	mkdir -p $(BUILD_DIR)/$(TOOLCHAIN_NAME)/DEBIAN
	mkdir -p $(BUILD_DIR)/$(TOOLCHAIN_NAME)/$(INSTALL_DIR)
	cp -r $(INSTALL_DIR)/* $(BUILD_DIR)/$(TOOLCHAIN_NAME)/$(INSTALL_DIR)
	echo "Package: $(TOOLCHAIN_NAME)" > $(DEB_CONTROL)
	echo "Version: $(GCC_VERSION)-$(RELEASE_NUMBER)" >> $(DEB_CONTROL)
	echo "Section: devel" >> $(DEB_CONTROL)
	echo "Priority: optional" >> $(DEB_CONTROL)
	echo "Architecture: all" >> $(DEB_CONTROL)
	echo "Depends: libc6" >> $(DEB_CONTROL)
	echo "Maintainer: $(MAINTAINER)" >> $(DEB_CONTROL)
	echo "Description: GNU toolchain" >> $(DEB_CONTROL)
	dpkg-deb -b /$(BUILD_DIR)/$(TOOLCHAIN_NAME) x86_64/$(TOOLCHAIN_NAME)-$(GCC_VERSION)-$(RELEASE_NUMBER).x86_64.deb

pack-rpm:
	mkdir -p $(BUILD_DIR)/buildroot/$(INSTALL_DIR)
	cp -r $(INSTALL_DIR)/* $(BUILD_DIR)/buildroot/$(INSTALL_DIR)
	echo "Summary: GNU toolchain" > $(RPM_SPEC)
	echo "Name: $(TOOLCHAIN_NAME)" >> $(RPM_SPEC)
	echo "Version: $(GCC_VERSION)" >> $(RPM_SPEC)
	echo "Release: $(RELEASE_NUMBER)" >> $(RPM_SPEC)
	echo "License: GPL" >> $(RPM_SPEC)
	echo "Group: Development/Tools" >> $(RPM_SPEC)
	echo "AutoReqProv: no" >> $(RPM_SPEC)
	echo "%define _source_filedigest_algorithm md5" >> $(RPM_SPEC)
	echo "%define _binary_filedigest_algorithm md5" >> $(RPM_SPEC)
	echo "%define _build_id_links none" >> $(RPM_SPEC)
	echo "%define _rpmdir ." >> $(RPM_SPEC)
	echo "%description" >> $(RPM_SPEC)
	echo "%files" >> $(RPM_SPEC)
	echo "%defattr(-,root,root)" >> $(RPM_SPEC)
	echo "/*" >> $(RPM_SPEC)
	rpmbuild -bb --buildroot=$(BUILD_DIR)/buildroot $(RPM_SPEC)

clean:
	rm -rf $(BUILD_DIR)
